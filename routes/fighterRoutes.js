const { Router } = require('express');
const FighterService = require('../services/fighterService');
const { responseMiddleware } = require('../middlewares/response.middleware');
const { createFighterValid, updateFighterValid } = require('../middlewares/fighter.validation.middleware');

const router = Router();

// TODO: Implement route controllers for fighter

router.get('/', function(req, res, next) {
    const opertation = FighterService.get();
    res.send(opertation);
});

router.get('/:id', function(req, res, next) {
    const opertation = FighterService.getByid(req.params.id);
    res.send(opertation);
});

router.post('/', createFighterValid, function(req, res, next) {

    try {const opertation = FighterService.create(req.body);
        res.send(opertation);} catch (err){
            responseMiddleware(err,req,res,next);
    }
});

router.put('/:id', updateFighterValid, function(req, res, next) {
    const opertation = FighterService.update(req.params.id,req.body);
    res.send(opertation);
});

router.delete('/:id', function(req, res, next) {
    const opertation = FighterService.delete(req.params.id);
    res.send(opertation);
});

module.exports = router;