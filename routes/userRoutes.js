const { Router } = require('express');
const UserService = require('../services/userService');
const { createUserValid, updateUserValid } = require('../middlewares/user.validation.middleware');
const { responseMiddleware } = require('../middlewares/response.middleware');

const router = Router();

// TODO: Implement route controllers for user

router.get('/', function(req, res, next) {
    const opertation = UserService.get();
    res.send(opertation);
});

router.get('/:id', function(req, res, next) {
    const opertation = UserService.getByid(req.params.id);
    res.send(opertation);
});

router.post('/', [createUserValid,responseMiddleware], function(req, res, next) {
    try {const opertation = UserService.create(req.body);
    res.send(opertation);} catch (err){
        responseMiddleware(err,req,res,next);
    }
});

router.put('/:id',  updateUserValid, function(req, res, next) {
    const opertation = UserService.update(req.params.id,req.body);
    res.send(opertation);
});

router.delete('/:id', function(req, res, next) {
    const opertation = UserService.delete(req.params.id);
    res.send(opertation);
});


module.exports = router;