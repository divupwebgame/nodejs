const responseMiddleware = (err,req, res, next) => {
   // TODO: Implement middleware that returns result of the query
   res.status(err.status).send({
      error: true,
      message: err.message  });
}

exports.responseMiddleware = responseMiddleware;