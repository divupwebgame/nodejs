const { user } = require('../models/user');
const createUserValid = (req, res, next) => {
    // TODO: Implement validatior for user entity during creation
    console.log("Creation user:");
    const firstName = req.body.firstName;
    const lastName = req.body.lastName;
    const email = req.body.email;
    const phoneNumber = req.body.phoneNumber;
    const password = req.body.password;
    
    function emailValidation(email){
      return email.match('^[a-z0-9](\.?[a-z0-9]){1,}@g(oogle)?mail\.com$');
    }
    

    function phoneValidation(phoneNumber){
      return phoneNumber.match('\s{0,}380\s{0,}[0-9]{9,9}');
    }

    function passwordValidation(password){
        return password.length>3;
    }

    if(firstName && lastName && email && phoneNumber && password){
        if(typeof req.body.id=='undefined'){
            if(phoneValidation(phoneNumber) && passwordValidation(password) && emailValidation(email)){
                next();
            } else {
               const error = new Error('Incorrect field values.');
               error.status = 400;
               throw error;
            }
            
        } else{
            const error = new Error('Note: Id value is not accepted.');
            error.status = 400;
            throw error;
        }
        
    } else {
        const error = new Error('Note: Not all fields are filled.');
        error.status = 400;
        throw error;
    }

}

const updateUserValid = (req, res, next) => {
    // TODO: Implement validatior for user entity during update
    const firstName = req.body.firstName;
    const lastName = req.body.lastName;
    const email = req.body.email;
    const phoneNumber = req.body.phoneNumber;
    const password = req.body.password;

    function emailValidation(email){
        return email.match('^[a-z0-9](\.?[a-z0-9]){1,}@g(oogle)?mail\.com$');
    }
      
  
    function phoneValidation(phoneNumber){
        return phoneNumber.match('\s{0,}380\s{0,}[0-9]{9,9}');
    }
  
    function passwordValidation(password){
          return password.length>3;
    }

    if( firstName || lastName || email || phoneNumber || password){
        if(phoneValidation(phoneNumber) && passwordValidation(password) && emailValidation(email)){
            next();
        } else {
            const error = new Error('Incorrect field values.');
            error.status = 400;
            throw error;
        }
    } else {
        const error = new Error('Note: No value to update.');
        error.status = 400;
        throw error;
    }
 
}

exports.createUserValid = createUserValid;
exports.updateUserValid = updateUserValid;