const { fighter } = require('../models/fighter');

const createFighterValid = (req, res, next) => {
   
    console.log("Creation fighter:");
    const name= req.body.name;
    const power = req.body.power;
    const defense = req.body.defense;
   
    function powerValidation(power){
        return (1<power) && (power<100) ? true : false;
    }

    function defenseValidation(defense){
        return (1<defense) && (defense<10) ? true : false;
    }

    if(name && power && defense){
        if(typeof req.body.id=='undefined' && typeof req.body.health=='undefined'){
            if(powerValidation(power) && defenseValidation(defense)){
                next();
            } else {
                const error = new Error('Incorrect field values.');
                error.status = 400;
                throw error;
            }

        } else{
            const error = new Error('Note: id or helath value are not accepted.');
            error.status = 400;
            throw error;
        }
        
    } else {
        const error = new Error('Note: Not all fields are filled.');
        error.status = 400;
        throw error;
    }

}

const updateFighterValid = (req, res, next) => {
    // TODO: Implement validatior for fighter entity during update

    const name= req.body.name;
    const power = req.body.power;
    const defense = req.body.defense;
    const health = req.body.health;

    function healthValidation(health){
        return (80<health) && (health<120) ? true : false;
    } 

    if(name || power || defense || health){
        if(healthValidation(health) && defenseValidation(defense)){
            next();
        } else {
            const error = new Error('Incorrect field values.');
            error.status = 400;
            throw error;
            
        }
    } else {
        const error = new Error('Note: No value to update.');
        error.status = 400;
        throw error;
    }

   
}

exports.createFighterValid = createFighterValid;
exports.updateFighterValid = updateFighterValid;