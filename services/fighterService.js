const { FighterRepository } = require('../repositories/fighterRepository');

class FighterService {
    // TODO: Implement methods to work with fighters

    get(){
        return FighterRepository.getAll();
      }
  
      getByid(id){
        return FighterRepository.getOne({id});
      }
  
      create(user){

        const error = new Error('Duplicate field values.');
        error.status = 400;
        if( FighterRepository.getOne({name: user.name}))
          throw error
        return FighterRepository.create(user);
      }
  
      update(id, date){
          return FighterRepository.update(id,date)
      }
  
      search(search) {
          const item = FighterRepository.getOne(search);
          if(!item) {
              return null;
          }
          return item;
      }
  
      delete(id){
        return FighterRepository.delete(id);
      }

}

module.exports = new FighterService();