const { UserRepository } = require('../repositories/userRepository');

class UserService {

    // TODO: Implement methods to work with user
    get(){
      return UserRepository.getAll();
    }

    getByid(id){
      return UserRepository.getOne({id});
    }

    create(user){
      const error = new Error('Duplicate field values.');
      error.status = 400;
      if( UserRepository.getOne({email: user.email}) || UserRepository.getOne({phoneNumber: user.phoneNumber}))
        throw error
      return UserRepository.create(user);
    }

    update(id, date){
        return UserRepository.update(id,date)
    }

    search(search) {
        const item = UserRepository.getOne(search);
        if(!item) {
            return null;
        }
        return item;
    }

    delete(id){
      return UserRepository.delete(id);
    }
}

module.exports = new UserService();